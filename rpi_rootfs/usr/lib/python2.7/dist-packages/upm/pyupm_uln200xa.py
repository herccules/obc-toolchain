# This file was automatically generated by SWIG (http://www.swig.org).
# Version 3.0.10
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.





from sys import version_info as _swig_python_version_info
if _swig_python_version_info >= (2, 7, 0):
    def swig_import_helper():
        import importlib
        pkg = __name__.rpartition('.')[0]
        mname = '.'.join((pkg, '_pyupm_uln200xa')).lstrip('.')
        try:
            return importlib.import_module(mname)
        except ImportError:
            return importlib.import_module('_pyupm_uln200xa')
    _pyupm_uln200xa = swig_import_helper()
    del swig_import_helper
elif _swig_python_version_info >= (2, 6, 0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_pyupm_uln200xa', [dirname(__file__)])
        except ImportError:
            import _pyupm_uln200xa
            return _pyupm_uln200xa
        if fp is not None:
            try:
                _mod = imp.load_module('_pyupm_uln200xa', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _pyupm_uln200xa = swig_import_helper()
    del swig_import_helper
else:
    import _pyupm_uln200xa
del _swig_python_version_info
try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if (name == "thisown"):
        return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if (not static):
        if _newclass:
            object.__setattr__(self, name, value)
        else:
            self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr(self, class_type, name):
    if (name == "thisown"):
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    raise AttributeError("'%s' object has no attribute '%s'" % (class_type.__name__, name))


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except __builtin__.Exception:
    class _object:
        pass
    _newclass = 0


def getVersion():
    return _pyupm_uln200xa.getVersion()
getVersion = _pyupm_uln200xa.getVersion
ULN200XA_DIR_CW = _pyupm_uln200xa.ULN200XA_DIR_CW
ULN200XA_DIR_CCW = _pyupm_uln200xa.ULN200XA_DIR_CCW
class ULN200XA(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, ULN200XA, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, ULN200XA, name)
    __repr__ = _swig_repr

    def __init__(self, stepsPerRev, i1, i2, i3, i4):
        this = _pyupm_uln200xa.new_ULN200XA(stepsPerRev, i1, i2, i3, i4)
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _pyupm_uln200xa.delete_ULN200XA
    __del__ = lambda self: None

    def setSpeed(self, speed):
        return _pyupm_uln200xa.ULN200XA_setSpeed(self, speed)

    def setDirection(self, dir):
        return _pyupm_uln200xa.ULN200XA_setDirection(self, dir)

    def stepperSteps(self, steps):
        return _pyupm_uln200xa.ULN200XA_stepperSteps(self, steps)

    def release(self):
        return _pyupm_uln200xa.ULN200XA_release(self)
ULN200XA_swigregister = _pyupm_uln200xa.ULN200XA_swigregister
ULN200XA_swigregister(ULN200XA)

# This file is compatible with both classic and new-style classes.


