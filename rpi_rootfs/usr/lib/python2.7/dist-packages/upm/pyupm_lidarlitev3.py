# This file was automatically generated by SWIG (http://www.swig.org).
# Version 3.0.10
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.





from sys import version_info as _swig_python_version_info
if _swig_python_version_info >= (2, 7, 0):
    def swig_import_helper():
        import importlib
        pkg = __name__.rpartition('.')[0]
        mname = '.'.join((pkg, '_pyupm_lidarlitev3')).lstrip('.')
        try:
            return importlib.import_module(mname)
        except ImportError:
            return importlib.import_module('_pyupm_lidarlitev3')
    _pyupm_lidarlitev3 = swig_import_helper()
    del swig_import_helper
elif _swig_python_version_info >= (2, 6, 0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_pyupm_lidarlitev3', [dirname(__file__)])
        except ImportError:
            import _pyupm_lidarlitev3
            return _pyupm_lidarlitev3
        if fp is not None:
            try:
                _mod = imp.load_module('_pyupm_lidarlitev3', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _pyupm_lidarlitev3 = swig_import_helper()
    del swig_import_helper
else:
    import _pyupm_lidarlitev3
del _swig_python_version_info
try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if (name == "thisown"):
        return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if (not static):
        if _newclass:
            object.__setattr__(self, name, value)
        else:
            self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr(self, class_type, name):
    if (name == "thisown"):
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    raise AttributeError("'%s' object has no attribute '%s'" % (class_type.__name__, name))


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except __builtin__.Exception:
    class _object:
        pass
    _newclass = 0


def getVersion():
    return _pyupm_lidarlitev3.getVersion()
getVersion = _pyupm_lidarlitev3.getVersion
ADDR = _pyupm_lidarlitev3.ADDR
ACQ_COMMAND = _pyupm_lidarlitev3.ACQ_COMMAND
STATUS = _pyupm_lidarlitev3.STATUS
SIG_COUNT_VAL = _pyupm_lidarlitev3.SIG_COUNT_VAL
ACQ_CONFIG_REG = _pyupm_lidarlitev3.ACQ_CONFIG_REG
VELOCITY = _pyupm_lidarlitev3.VELOCITY
PEAK_CORR = _pyupm_lidarlitev3.PEAK_CORR
NOISE_PEAK = _pyupm_lidarlitev3.NOISE_PEAK
SIGNAL_STRENGTH = _pyupm_lidarlitev3.SIGNAL_STRENGTH
FULL_DELAY_HIGH = _pyupm_lidarlitev3.FULL_DELAY_HIGH
FULL_DELAY_LOW = _pyupm_lidarlitev3.FULL_DELAY_LOW
OUTER_LOOP_COUNT = _pyupm_lidarlitev3.OUTER_LOOP_COUNT
REF_COUNT_VAL = _pyupm_lidarlitev3.REF_COUNT_VAL
LAST_DELAY_HIGH = _pyupm_lidarlitev3.LAST_DELAY_HIGH
LAST_DELAY_LOW = _pyupm_lidarlitev3.LAST_DELAY_LOW
UNIT_ID_HIGH = _pyupm_lidarlitev3.UNIT_ID_HIGH
UNIT_ID_LOW = _pyupm_lidarlitev3.UNIT_ID_LOW
I2C_ID_HIGH = _pyupm_lidarlitev3.I2C_ID_HIGH
I2C_ID_LOW = _pyupm_lidarlitev3.I2C_ID_LOW
I2C_SEC_ADDR = _pyupm_lidarlitev3.I2C_SEC_ADDR
THRESHOLD_BYPASS = _pyupm_lidarlitev3.THRESHOLD_BYPASS
I2C_CONFIG = _pyupm_lidarlitev3.I2C_CONFIG
COMMAND = _pyupm_lidarlitev3.COMMAND
MEASURE_DELAY = _pyupm_lidarlitev3.MEASURE_DELAY
PEAK_BCK = _pyupm_lidarlitev3.PEAK_BCK
CORR_DATA = _pyupm_lidarlitev3.CORR_DATA
CORR_DATA_SIGN = _pyupm_lidarlitev3.CORR_DATA_SIGN
ACQ_SETTINGS = _pyupm_lidarlitev3.ACQ_SETTINGS
POWER_CONTROL = _pyupm_lidarlitev3.POWER_CONTROL
HIGH = _pyupm_lidarlitev3.HIGH
LOW = _pyupm_lidarlitev3.LOW
class LIDARLITEV3(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, LIDARLITEV3, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, LIDARLITEV3, name)
    __repr__ = _swig_repr

    def __init__(self, bus, devAddr=0x62):
        this = _pyupm_lidarlitev3.new_LIDARLITEV3(bus, devAddr)
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this

    def getDistance(self):
        return _pyupm_lidarlitev3.LIDARLITEV3_getDistance(self)

    def read(self, reg, monitorBusyFlag):
        return _pyupm_lidarlitev3.LIDARLITEV3_read(self, reg, monitorBusyFlag)

    def name(self):
        return _pyupm_lidarlitev3.LIDARLITEV3_name(self)

    def i2cReadReg_8(self, reg):
        return _pyupm_lidarlitev3.LIDARLITEV3_i2cReadReg_8(self, reg)

    def i2cReadReg_16(self, reg):
        return _pyupm_lidarlitev3.LIDARLITEV3_i2cReadReg_16(self, reg)

    def i2cWriteReg(self, reg, value):
        return _pyupm_lidarlitev3.LIDARLITEV3_i2cWriteReg(self, reg, value)
    __swig_destroy__ = _pyupm_lidarlitev3.delete_LIDARLITEV3
    __del__ = lambda self: None
LIDARLITEV3_swigregister = _pyupm_lidarlitev3.LIDARLITEV3_swigregister
LIDARLITEV3_swigregister(LIDARLITEV3)

# This file is compatible with both classic and new-style classes.


