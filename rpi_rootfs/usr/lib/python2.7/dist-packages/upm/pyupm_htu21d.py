# This file was automatically generated by SWIG (http://www.swig.org).
# Version 3.0.10
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.





from sys import version_info as _swig_python_version_info
if _swig_python_version_info >= (2, 7, 0):
    def swig_import_helper():
        import importlib
        pkg = __name__.rpartition('.')[0]
        mname = '.'.join((pkg, '_pyupm_htu21d')).lstrip('.')
        try:
            return importlib.import_module(mname)
        except ImportError:
            return importlib.import_module('_pyupm_htu21d')
    _pyupm_htu21d = swig_import_helper()
    del swig_import_helper
elif _swig_python_version_info >= (2, 6, 0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_pyupm_htu21d', [dirname(__file__)])
        except ImportError:
            import _pyupm_htu21d
            return _pyupm_htu21d
        if fp is not None:
            try:
                _mod = imp.load_module('_pyupm_htu21d', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _pyupm_htu21d = swig_import_helper()
    del swig_import_helper
else:
    import _pyupm_htu21d
del _swig_python_version_info
try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if (name == "thisown"):
        return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if (not static):
        if _newclass:
            object.__setattr__(self, name, value)
        else:
            self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr(self, class_type, name):
    if (name == "thisown"):
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    raise AttributeError("'%s' object has no attribute '%s'" % (class_type.__name__, name))


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except __builtin__.Exception:
    class _object:
        pass
    _newclass = 0


def getVersion():
    return _pyupm_htu21d.getVersion()
getVersion = _pyupm_htu21d.getVersion
HTU21D_NAME = _pyupm_htu21d.HTU21D_NAME
HTU21D_I2C_ADDRESS = _pyupm_htu21d.HTU21D_I2C_ADDRESS
HTU21D_READ_TEMP_HOLD = _pyupm_htu21d.HTU21D_READ_TEMP_HOLD
HTU21D_READ_HUMIDITY_HOLD = _pyupm_htu21d.HTU21D_READ_HUMIDITY_HOLD
HTU21D_WRITE_USER_REG = _pyupm_htu21d.HTU21D_WRITE_USER_REG
HTU21D_READ_USER_REG = _pyupm_htu21d.HTU21D_READ_USER_REG
HTU21D_SOFT_RESET = _pyupm_htu21d.HTU21D_SOFT_RESET
HTU21D_DISABLE_OTP = _pyupm_htu21d.HTU21D_DISABLE_OTP
HTU21D_HEATER_ENABLE = _pyupm_htu21d.HTU21D_HEATER_ENABLE
HTU21D_END_OF_BATTERY = _pyupm_htu21d.HTU21D_END_OF_BATTERY
HTU21D_RESO_RH12_T14 = _pyupm_htu21d.HTU21D_RESO_RH12_T14
HTU21D_RESO_RH8_T12 = _pyupm_htu21d.HTU21D_RESO_RH8_T12
HTU21D_RESO_RH10_T13 = _pyupm_htu21d.HTU21D_RESO_RH10_T13
HTU21D_RESO_RH11_T11 = _pyupm_htu21d.HTU21D_RESO_RH11_T11
class HTU21D(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, HTU21D, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, HTU21D, name)
    __repr__ = _swig_repr

    def __init__(self, bus, devAddr=0x40):
        this = _pyupm_htu21d.new_HTU21D(bus, devAddr)
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this

    def sampleData(self):
        return _pyupm_htu21d.HTU21D_sampleData(self)

    def getHumidity(self, bSampleData=False):
        return _pyupm_htu21d.HTU21D_getHumidity(self, bSampleData)

    def getTemperature(self, bSampleData=False):
        return _pyupm_htu21d.HTU21D_getTemperature(self, bSampleData)

    def getCompRH(self, bSampleData=False):
        return _pyupm_htu21d.HTU21D_getCompRH(self, bSampleData)

    def getDewPoint(self, bSampleData=False):
        return _pyupm_htu21d.HTU21D_getDewPoint(self, bSampleData)

    def getHumidityData(self, fHum, fHumTemp, fDewPt):
        return _pyupm_htu21d.HTU21D_getHumidityData(self, fHum, fHumTemp, fDewPt)

    def setHeater(self, bEnable=False):
        return _pyupm_htu21d.HTU21D_setHeater(self, bEnable)

    def resetSensor(self):
        return _pyupm_htu21d.HTU21D_resetSensor(self)

    def testSensor(self):
        return _pyupm_htu21d.HTU21D_testSensor(self)

    def i2cWriteReg(self, reg, value):
        return _pyupm_htu21d.HTU21D_i2cWriteReg(self, reg, value)

    def i2cReadReg_16(self, reg):
        return _pyupm_htu21d.HTU21D_i2cReadReg_16(self, reg)

    def i2cReadReg_8(self, reg):
        return _pyupm_htu21d.HTU21D_i2cReadReg_8(self, reg)
    __swig_destroy__ = _pyupm_htu21d.delete_HTU21D
    __del__ = lambda self: None
HTU21D_swigregister = _pyupm_htu21d.HTU21D_swigregister
HTU21D_swigregister(HTU21D)

# This file is compatible with both classic and new-style classes.


