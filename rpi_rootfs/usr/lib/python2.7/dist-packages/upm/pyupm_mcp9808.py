# This file was automatically generated by SWIG (http://www.swig.org).
# Version 3.0.10
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.





from sys import version_info as _swig_python_version_info
if _swig_python_version_info >= (2, 7, 0):
    def swig_import_helper():
        import importlib
        pkg = __name__.rpartition('.')[0]
        mname = '.'.join((pkg, '_pyupm_mcp9808')).lstrip('.')
        try:
            return importlib.import_module(mname)
        except ImportError:
            return importlib.import_module('_pyupm_mcp9808')
    _pyupm_mcp9808 = swig_import_helper()
    del swig_import_helper
elif _swig_python_version_info >= (2, 6, 0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_pyupm_mcp9808', [dirname(__file__)])
        except ImportError:
            import _pyupm_mcp9808
            return _pyupm_mcp9808
        if fp is not None:
            try:
                _mod = imp.load_module('_pyupm_mcp9808', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _pyupm_mcp9808 = swig_import_helper()
    del swig_import_helper
else:
    import _pyupm_mcp9808
del _swig_python_version_info
try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if (name == "thisown"):
        return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if (not static):
        if _newclass:
            object.__setattr__(self, name, value)
        else:
            self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr(self, class_type, name):
    if (name == "thisown"):
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    raise AttributeError("'%s' object has no attribute '%s'" % (class_type.__name__, name))


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except __builtin__.Exception:
    class _object:
        pass
    _newclass = 0


def getVersion():
    return _pyupm_mcp9808.getVersion()
getVersion = _pyupm_mcp9808.getVersion
MCP9808_REG_CONFIG = _pyupm_mcp9808.MCP9808_REG_CONFIG
MCP9808_REG_AMBIENT_TEMP = _pyupm_mcp9808.MCP9808_REG_AMBIENT_TEMP
MCP9808_REG_MANUF_ID = _pyupm_mcp9808.MCP9808_REG_MANUF_ID
MCP9808_REG_DEVICE_ID = _pyupm_mcp9808.MCP9808_REG_DEVICE_ID
MCP9808_REG_RESOLUTION = _pyupm_mcp9808.MCP9808_REG_RESOLUTION
MCP9808_CONFIG_SHUTDOWN = _pyupm_mcp9808.MCP9808_CONFIG_SHUTDOWN
MCP9808_CONFIG_CRITLOCKED = _pyupm_mcp9808.MCP9808_CONFIG_CRITLOCKED
MCP9808_CONFIG_WINLOCKED = _pyupm_mcp9808.MCP9808_CONFIG_WINLOCKED
MCP9808_CONFIG_INTCLR = _pyupm_mcp9808.MCP9808_CONFIG_INTCLR
class MCP9808(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, MCP9808, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, MCP9808, name)
    __repr__ = _swig_repr
    UPPER_TEMP = _pyupm_mcp9808.MCP9808_UPPER_TEMP
    LOWER_TEMP = _pyupm_mcp9808.MCP9808_LOWER_TEMP
    CRIT_TEMP = _pyupm_mcp9808.MCP9808_CRIT_TEMP
    RES_LOW = _pyupm_mcp9808.MCP9808_RES_LOW
    RES_MEDIUM = _pyupm_mcp9808.MCP9808_RES_MEDIUM
    RES_HIGH = _pyupm_mcp9808.MCP9808_RES_HIGH
    RES_PRECISION = _pyupm_mcp9808.MCP9808_RES_PRECISION
    ALERTSTAT = _pyupm_mcp9808.MCP9808_ALERTSTAT
    ALERTCTRL = _pyupm_mcp9808.MCP9808_ALERTCTRL
    ALERTSEL = _pyupm_mcp9808.MCP9808_ALERTSEL
    ALERTPOL = _pyupm_mcp9808.MCP9808_ALERTPOL
    ALERTMODE = _pyupm_mcp9808.MCP9808_ALERTMODE
    HYST_0 = _pyupm_mcp9808.MCP9808_HYST_0
    HYST_1_5 = _pyupm_mcp9808.MCP9808_HYST_1_5
    HYST_3_0 = _pyupm_mcp9808.MCP9808_HYST_3_0
    HYST_6_0 = _pyupm_mcp9808.MCP9808_HYST_6_0

    def __init__(self, bus, address=0x18):
        this = _pyupm_mcp9808.new_MCP9808(bus, address)
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _pyupm_mcp9808.delete_MCP9808
    __del__ = lambda self: None

    def name(self):
        return _pyupm_mcp9808.MCP9808_name(self)

    def getTemp(self):
        return _pyupm_mcp9808.MCP9808_getTemp(self)

    def shutDown(self, sleep=True):
        return _pyupm_mcp9808.MCP9808_shutDown(self, sleep)

    def setMode(self, celsius=True):
        return _pyupm_mcp9808.MCP9808_setMode(self, celsius)

    def isCelsius(self):
        return _pyupm_mcp9808.MCP9808_isCelsius(self)

    def isTcrit(self):
        return _pyupm_mcp9808.MCP9808_isTcrit(self)

    def isTupper(self):
        return _pyupm_mcp9808.MCP9808_isTupper(self)

    def isTlower(self):
        return _pyupm_mcp9808.MCP9808_isTlower(self)

    def setMonitorReg(self, reg, value):
        return _pyupm_mcp9808.MCP9808_setMonitorReg(self, reg, value)

    def getMonitorReg(self, reg):
        return _pyupm_mcp9808.MCP9808_getMonitorReg(self, reg)

    def clearInterrupt(self):
        return _pyupm_mcp9808.MCP9808_clearInterrupt(self)

    def setAlertMode(self, command):
        return _pyupm_mcp9808.MCP9808_setAlertMode(self, command)

    def clearAlertMode(self):
        return _pyupm_mcp9808.MCP9808_clearAlertMode(self)

    def setHysteresis(self, value):
        return _pyupm_mcp9808.MCP9808_setHysteresis(self, value)

    def getHysteresis(self):
        return _pyupm_mcp9808.MCP9808_getHysteresis(self)

    def setResolution(self, value):
        return _pyupm_mcp9808.MCP9808_setResolution(self, value)

    def getResolution(self):
        return _pyupm_mcp9808.MCP9808_getResolution(self)

    def getManufacturer(self):
        return _pyupm_mcp9808.MCP9808_getManufacturer(self)

    def getDevicedId(self):
        return _pyupm_mcp9808.MCP9808_getDevicedId(self)
MCP9808_swigregister = _pyupm_mcp9808.MCP9808_swigregister
MCP9808_swigregister(MCP9808)

# This file is compatible with both classic and new-style classes.


