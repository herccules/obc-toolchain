# This file was automatically generated by SWIG (http://www.swig.org).
# Version 3.0.10
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.





from sys import version_info as _swig_python_version_info
if _swig_python_version_info >= (2, 7, 0):
    def swig_import_helper():
        import importlib
        pkg = __name__.rpartition('.')[0]
        mname = '.'.join((pkg, '_pyupm_mpr121')).lstrip('.')
        try:
            return importlib.import_module(mname)
        except ImportError:
            return importlib.import_module('_pyupm_mpr121')
    _pyupm_mpr121 = swig_import_helper()
    del swig_import_helper
elif _swig_python_version_info >= (2, 6, 0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_pyupm_mpr121', [dirname(__file__)])
        except ImportError:
            import _pyupm_mpr121
            return _pyupm_mpr121
        if fp is not None:
            try:
                _mod = imp.load_module('_pyupm_mpr121', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _pyupm_mpr121 = swig_import_helper()
    del swig_import_helper
else:
    import _pyupm_mpr121
del _swig_python_version_info
try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if (name == "thisown"):
        return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if (not static):
        if _newclass:
            object.__setattr__(self, name, value)
        else:
            self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr(self, class_type, name):
    if (name == "thisown"):
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    raise AttributeError("'%s' object has no attribute '%s'" % (class_type.__name__, name))


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except __builtin__.Exception:
    class _object:
        pass
    _newclass = 0


def getVersion():
    return _pyupm_mpr121.getVersion()
getVersion = _pyupm_mpr121.getVersion
MPR121_I2C_BUS = _pyupm_mpr121.MPR121_I2C_BUS
MPR121_DEFAULT_I2C_ADDR = _pyupm_mpr121.MPR121_DEFAULT_I2C_ADDR
class MPR121(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, MPR121, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, MPR121, name)
    __repr__ = _swig_repr

    def __init__(self, bus, address=0x5a):
        this = _pyupm_mpr121.new_MPR121(bus, address)
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this

    def configAN3944(self):
        return _pyupm_mpr121.MPR121_configAN3944(self)

    def readButtons(self):
        return _pyupm_mpr121.MPR121_readButtons(self)

    def writeBytes(self, reg, buffer, len):
        return _pyupm_mpr121.MPR121_writeBytes(self, reg, buffer, len)

    def readBytes(self, reg, buffer, len):
        return _pyupm_mpr121.MPR121_readBytes(self, reg, buffer, len)
    __swig_setmethods__["m_buttonStates"] = _pyupm_mpr121.MPR121_m_buttonStates_set
    __swig_getmethods__["m_buttonStates"] = _pyupm_mpr121.MPR121_m_buttonStates_get
    if _newclass:
        m_buttonStates = _swig_property(_pyupm_mpr121.MPR121_m_buttonStates_get, _pyupm_mpr121.MPR121_m_buttonStates_set)
    __swig_setmethods__["m_overCurrentFault"] = _pyupm_mpr121.MPR121_m_overCurrentFault_set
    __swig_getmethods__["m_overCurrentFault"] = _pyupm_mpr121.MPR121_m_overCurrentFault_get
    if _newclass:
        m_overCurrentFault = _swig_property(_pyupm_mpr121.MPR121_m_overCurrentFault_get, _pyupm_mpr121.MPR121_m_overCurrentFault_set)
    __swig_destroy__ = _pyupm_mpr121.delete_MPR121
    __del__ = lambda self: None
MPR121_swigregister = _pyupm_mpr121.MPR121_swigregister
MPR121_swigregister(MPR121)

# This file is compatible with both classic and new-style classes.


