# This file was automatically generated by SWIG (http://www.swig.org).
# Version 3.0.10
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.





from sys import version_info as _swig_python_version_info
if _swig_python_version_info >= (2, 7, 0):
    def swig_import_helper():
        import importlib
        pkg = __name__.rpartition('.')[0]
        mname = '.'.join((pkg, '_pyupm_st7735')).lstrip('.')
        try:
            return importlib.import_module(mname)
        except ImportError:
            return importlib.import_module('_pyupm_st7735')
    _pyupm_st7735 = swig_import_helper()
    del swig_import_helper
elif _swig_python_version_info >= (2, 6, 0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_pyupm_st7735', [dirname(__file__)])
        except ImportError:
            import _pyupm_st7735
            return _pyupm_st7735
        if fp is not None:
            try:
                _mod = imp.load_module('_pyupm_st7735', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _pyupm_st7735 = swig_import_helper()
    del swig_import_helper
else:
    import _pyupm_st7735
del _swig_python_version_info
try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if (name == "thisown"):
        return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if (not static):
        if _newclass:
            object.__setattr__(self, name, value)
        else:
            self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr(self, class_type, name):
    if (name == "thisown"):
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    raise AttributeError("'%s' object has no attribute '%s'" % (class_type.__name__, name))


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except __builtin__.Exception:
    class _object:
        pass
    _newclass = 0


def getVersion():
    return _pyupm_st7735.getVersion()
getVersion = _pyupm_st7735.getVersion
class uint8Array(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, uint8Array, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, uint8Array, name)
    __repr__ = _swig_repr

    def __init__(self, nelements):
        this = _pyupm_st7735.new_uint8Array(nelements)
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _pyupm_st7735.delete_uint8Array
    __del__ = lambda self: None

    def __getitem__(self, index):
        return _pyupm_st7735.uint8Array___getitem__(self, index)

    def __setitem__(self, index, value):
        return _pyupm_st7735.uint8Array___setitem__(self, index, value)

    def cast(self):
        return _pyupm_st7735.uint8Array_cast(self)
    if _newclass:
        frompointer = staticmethod(_pyupm_st7735.uint8Array_frompointer)
    else:
        frompointer = _pyupm_st7735.uint8Array_frompointer
uint8Array_swigregister = _pyupm_st7735.uint8Array_swigregister
uint8Array_swigregister(uint8Array)

def uint8Array_frompointer(t):
    return _pyupm_st7735.uint8Array_frompointer(t)
uint8Array_frompointer = _pyupm_st7735.uint8Array_frompointer

class GFX(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, GFX, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, GFX, name)

    def __init__(self, *args, **kwargs):
        raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _pyupm_st7735.delete_GFX
    __del__ = lambda self: None

    def setAddrWindow(self, x0, y0, x1, y1):
        return _pyupm_st7735.GFX_setAddrWindow(self, x0, y0, x1, y1)

    def drawPixel(self, x, y, color):
        return _pyupm_st7735.GFX_drawPixel(self, x, y, color)

    def refresh(self):
        return _pyupm_st7735.GFX_refresh(self)

    def drawChar(self, x, y, data, color, bg, size):
        return _pyupm_st7735.GFX_drawChar(self, x, y, data, color, bg, size)

    def _print(self, msg):
        return _pyupm_st7735.GFX__print(self, msg)

    def setPixel(self, x, y, color):
        return _pyupm_st7735.GFX_setPixel(self, x, y, color)

    def fillScreen(self, color):
        return _pyupm_st7735.GFX_fillScreen(self, color)

    def fillRect(self, x, y, w, h, color):
        return _pyupm_st7735.GFX_fillRect(self, x, y, w, h, color)

    def drawFastVLine(self, x, y, h, color):
        return _pyupm_st7735.GFX_drawFastVLine(self, x, y, h, color)

    def drawLine(self, x0, y0, x1, y1, color):
        return _pyupm_st7735.GFX_drawLine(self, x0, y0, x1, y1, color)

    def drawTriangle(self, x0, y0, x1, y1, x2, y2, color):
        return _pyupm_st7735.GFX_drawTriangle(self, x0, y0, x1, y1, x2, y2, color)

    def drawCircle(self, x, y, r, color):
        return _pyupm_st7735.GFX_drawCircle(self, x, y, r, color)

    def setCursor(self, x, y):
        return _pyupm_st7735.GFX_setCursor(self, x, y)

    def setTextColor(self, textColor, textBGColor):
        return _pyupm_st7735.GFX_setTextColor(self, textColor, textBGColor)

    def setTextSize(self, size):
        return _pyupm_st7735.GFX_setTextSize(self, size)

    def setTextWrap(self, wrap):
        return _pyupm_st7735.GFX_setTextWrap(self, wrap)
    __swig_setmethods__["m_height"] = _pyupm_st7735.GFX_m_height_set
    __swig_getmethods__["m_height"] = _pyupm_st7735.GFX_m_height_get
    if _newclass:
        m_height = _swig_property(_pyupm_st7735.GFX_m_height_get, _pyupm_st7735.GFX_m_height_set)
    __swig_setmethods__["m_width"] = _pyupm_st7735.GFX_m_width_set
    __swig_getmethods__["m_width"] = _pyupm_st7735.GFX_m_width_get
    if _newclass:
        m_width = _swig_property(_pyupm_st7735.GFX_m_width_get, _pyupm_st7735.GFX_m_width_set)
    __swig_setmethods__["m_textSize"] = _pyupm_st7735.GFX_m_textSize_set
    __swig_getmethods__["m_textSize"] = _pyupm_st7735.GFX_m_textSize_get
    if _newclass:
        m_textSize = _swig_property(_pyupm_st7735.GFX_m_textSize_get, _pyupm_st7735.GFX_m_textSize_set)
    __swig_setmethods__["m_textColor"] = _pyupm_st7735.GFX_m_textColor_set
    __swig_getmethods__["m_textColor"] = _pyupm_st7735.GFX_m_textColor_get
    if _newclass:
        m_textColor = _swig_property(_pyupm_st7735.GFX_m_textColor_get, _pyupm_st7735.GFX_m_textColor_set)
    __swig_setmethods__["m_textBGColor"] = _pyupm_st7735.GFX_m_textBGColor_set
    __swig_getmethods__["m_textBGColor"] = _pyupm_st7735.GFX_m_textBGColor_get
    if _newclass:
        m_textBGColor = _swig_property(_pyupm_st7735.GFX_m_textBGColor_get, _pyupm_st7735.GFX_m_textBGColor_set)
    __swig_setmethods__["m_cursorX"] = _pyupm_st7735.GFX_m_cursorX_set
    __swig_getmethods__["m_cursorX"] = _pyupm_st7735.GFX_m_cursorX_get
    if _newclass:
        m_cursorX = _swig_property(_pyupm_st7735.GFX_m_cursorX_get, _pyupm_st7735.GFX_m_cursorX_set)
    __swig_setmethods__["m_cursorY"] = _pyupm_st7735.GFX_m_cursorY_set
    __swig_getmethods__["m_cursorY"] = _pyupm_st7735.GFX_m_cursorY_get
    if _newclass:
        m_cursorY = _swig_property(_pyupm_st7735.GFX_m_cursorY_get, _pyupm_st7735.GFX_m_cursorY_set)
    __swig_setmethods__["m_wrap"] = _pyupm_st7735.GFX_m_wrap_set
    __swig_getmethods__["m_wrap"] = _pyupm_st7735.GFX_m_wrap_get
    if _newclass:
        m_wrap = _swig_property(_pyupm_st7735.GFX_m_wrap_get, _pyupm_st7735.GFX_m_wrap_set)
    __swig_setmethods__["m_map"] = _pyupm_st7735.GFX_m_map_set
    __swig_getmethods__["m_map"] = _pyupm_st7735.GFX_m_map_get
    if _newclass:
        m_map = _swig_property(_pyupm_st7735.GFX_m_map_get, _pyupm_st7735.GFX_m_map_set)
GFX_swigregister = _pyupm_st7735.GFX_swigregister
GFX_swigregister(GFX)

INITR_GREENTAB = _pyupm_st7735.INITR_GREENTAB
INITR_REDTAB = _pyupm_st7735.INITR_REDTAB
INITR_BLACKTAB = _pyupm_st7735.INITR_BLACKTAB
ST7735_TFTWIDTH = _pyupm_st7735.ST7735_TFTWIDTH
ST7735_TFTHEIGHT = _pyupm_st7735.ST7735_TFTHEIGHT
ST7735_NOP = _pyupm_st7735.ST7735_NOP
ST7735_SWRESET = _pyupm_st7735.ST7735_SWRESET
ST7735_RDDID = _pyupm_st7735.ST7735_RDDID
ST7735_RDDST = _pyupm_st7735.ST7735_RDDST
ST7735_SLPIN = _pyupm_st7735.ST7735_SLPIN
ST7735_SLPOUT = _pyupm_st7735.ST7735_SLPOUT
ST7735_PTLON = _pyupm_st7735.ST7735_PTLON
ST7735_NORON = _pyupm_st7735.ST7735_NORON
ST7735_INVOFF = _pyupm_st7735.ST7735_INVOFF
ST7735_INVON = _pyupm_st7735.ST7735_INVON
ST7735_DISPOFF = _pyupm_st7735.ST7735_DISPOFF
ST7735_DISPON = _pyupm_st7735.ST7735_DISPON
ST7735_CASET = _pyupm_st7735.ST7735_CASET
ST7735_RASET = _pyupm_st7735.ST7735_RASET
ST7735_RAMWR = _pyupm_st7735.ST7735_RAMWR
ST7735_RAMRD = _pyupm_st7735.ST7735_RAMRD
ST7735_PTLAR = _pyupm_st7735.ST7735_PTLAR
ST7735_COLMOD = _pyupm_st7735.ST7735_COLMOD
ST7735_MADCTL = _pyupm_st7735.ST7735_MADCTL
ST7735_FRMCTR1 = _pyupm_st7735.ST7735_FRMCTR1
ST7735_FRMCTR2 = _pyupm_st7735.ST7735_FRMCTR2
ST7735_FRMCTR3 = _pyupm_st7735.ST7735_FRMCTR3
ST7735_INVCTR = _pyupm_st7735.ST7735_INVCTR
ST7735_DISSET5 = _pyupm_st7735.ST7735_DISSET5
ST7735_PWCTR1 = _pyupm_st7735.ST7735_PWCTR1
ST7735_PWCTR2 = _pyupm_st7735.ST7735_PWCTR2
ST7735_PWCTR3 = _pyupm_st7735.ST7735_PWCTR3
ST7735_PWCTR4 = _pyupm_st7735.ST7735_PWCTR4
ST7735_PWCTR5 = _pyupm_st7735.ST7735_PWCTR5
ST7735_VMCTR1 = _pyupm_st7735.ST7735_VMCTR1
ST7735_RDID1 = _pyupm_st7735.ST7735_RDID1
ST7735_RDID2 = _pyupm_st7735.ST7735_RDID2
ST7735_RDID3 = _pyupm_st7735.ST7735_RDID3
ST7735_RDID4 = _pyupm_st7735.ST7735_RDID4
ST7735_PWCTR6 = _pyupm_st7735.ST7735_PWCTR6
ST7735_GMCTRP1 = _pyupm_st7735.ST7735_GMCTRP1
ST7735_GMCTRN1 = _pyupm_st7735.ST7735_GMCTRN1
ST7735_BLACK = _pyupm_st7735.ST7735_BLACK
ST7735_BLUE = _pyupm_st7735.ST7735_BLUE
ST7735_RED = _pyupm_st7735.ST7735_RED
ST7735_GREEN = _pyupm_st7735.ST7735_GREEN
ST7735_CYAN = _pyupm_st7735.ST7735_CYAN
ST7735_MAGENTA = _pyupm_st7735.ST7735_MAGENTA
ST7735_YELLOW = _pyupm_st7735.ST7735_YELLOW
ST7735_WHITE = _pyupm_st7735.ST7735_WHITE
HIGH = _pyupm_st7735.HIGH
LOW = _pyupm_st7735.LOW
DELAY = _pyupm_st7735.DELAY
class ST7735(GFX):
    __swig_setmethods__ = {}
    for _s in [GFX]:
        __swig_setmethods__.update(getattr(_s, '__swig_setmethods__', {}))
    __setattr__ = lambda self, name, value: _swig_setattr(self, ST7735, name, value)
    __swig_getmethods__ = {}
    for _s in [GFX]:
        __swig_getmethods__.update(getattr(_s, '__swig_getmethods__', {}))
    __getattr__ = lambda self, name: _swig_getattr(self, ST7735, name)
    __repr__ = _swig_repr

    def __init__(self, csLCD, cSD, rs, rst):
        this = _pyupm_st7735.new_ST7735(csLCD, cSD, rs, rst)
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this

    def name(self):
        return _pyupm_st7735.ST7735_name(self)

    def initModule(self):
        return _pyupm_st7735.ST7735_initModule(self)

    def configModule(self):
        return _pyupm_st7735.ST7735_configModule(self)

    def write(self, value):
        return _pyupm_st7735.ST7735_write(self, value)

    def data(self, value):
        return _pyupm_st7735.ST7735_data(self, value)

    def executeCMDList(self, addr):
        return _pyupm_st7735.ST7735_executeCMDList(self, addr)

    def setAddrWindow(self, x0, y0, x1, y1):
        return _pyupm_st7735.ST7735_setAddrWindow(self, x0, y0, x1, y1)

    def drawPixel(self, x, y, color):
        return _pyupm_st7735.ST7735_drawPixel(self, x, y, color)

    def refresh(self):
        return _pyupm_st7735.ST7735_refresh(self)

    def lcdCSOn(self):
        return _pyupm_st7735.ST7735_lcdCSOn(self)

    def lcdCSOff(self):
        return _pyupm_st7735.ST7735_lcdCSOff(self)

    def sdCSOn(self):
        return _pyupm_st7735.ST7735_sdCSOn(self)

    def sdCSOff(self):
        return _pyupm_st7735.ST7735_sdCSOff(self)

    def rsHIGH(self):
        return _pyupm_st7735.ST7735_rsHIGH(self)

    def rsLOW(self):
        return _pyupm_st7735.ST7735_rsLOW(self)
    __swig_setmethods__["m_map"] = _pyupm_st7735.ST7735_m_map_set
    __swig_getmethods__["m_map"] = _pyupm_st7735.ST7735_m_map_get
    if _newclass:
        m_map = _swig_property(_pyupm_st7735.ST7735_m_map_get, _pyupm_st7735.ST7735_m_map_set)
    __swig_destroy__ = _pyupm_st7735.delete_ST7735
    __del__ = lambda self: None
ST7735_swigregister = _pyupm_st7735.ST7735_swigregister
ST7735_swigregister(ST7735)
cvar = _pyupm_st7735.cvar
Bcmd = cvar.Bcmd
Rcmd1 = cvar.Rcmd1
Rcmd2green = cvar.Rcmd2green
Rcmd2red = cvar.Rcmd2red
Rcmd3 = cvar.Rcmd3
font = cvar.font

# This file is compatible with both classic and new-style classes.


