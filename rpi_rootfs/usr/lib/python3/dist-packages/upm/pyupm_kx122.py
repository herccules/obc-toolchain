# This file was automatically generated by SWIG (http://www.swig.org).
# Version 3.0.10
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.





from sys import version_info as _swig_python_version_info
if _swig_python_version_info >= (2, 7, 0):
    def swig_import_helper():
        import importlib
        pkg = __name__.rpartition('.')[0]
        mname = '.'.join((pkg, '_pyupm_kx122')).lstrip('.')
        try:
            return importlib.import_module(mname)
        except ImportError:
            return importlib.import_module('_pyupm_kx122')
    _pyupm_kx122 = swig_import_helper()
    del swig_import_helper
elif _swig_python_version_info >= (2, 6, 0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_pyupm_kx122', [dirname(__file__)])
        except ImportError:
            import _pyupm_kx122
            return _pyupm_kx122
        if fp is not None:
            try:
                _mod = imp.load_module('_pyupm_kx122', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _pyupm_kx122 = swig_import_helper()
    del swig_import_helper
else:
    import _pyupm_kx122
del _swig_python_version_info
try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if (name == "thisown"):
        return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if (not static):
        if _newclass:
            object.__setattr__(self, name, value)
        else:
            self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr(self, class_type, name):
    if (name == "thisown"):
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    raise AttributeError("'%s' object has no attribute '%s'" % (class_type.__name__, name))


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except __builtin__.Exception:
    class _object:
        pass
    _newclass = 0


def getVersion():
    return _pyupm_kx122.getVersion()
getVersion = _pyupm_kx122.getVersion
class SwigPyIterator(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, SwigPyIterator, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, SwigPyIterator, name)

    def __init__(self, *args, **kwargs):
        raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _pyupm_kx122.delete_SwigPyIterator
    __del__ = lambda self: None

    def value(self):
        return _pyupm_kx122.SwigPyIterator_value(self)

    def incr(self, n=1):
        return _pyupm_kx122.SwigPyIterator_incr(self, n)

    def decr(self, n=1):
        return _pyupm_kx122.SwigPyIterator_decr(self, n)

    def distance(self, x):
        return _pyupm_kx122.SwigPyIterator_distance(self, x)

    def equal(self, x):
        return _pyupm_kx122.SwigPyIterator_equal(self, x)

    def copy(self):
        return _pyupm_kx122.SwigPyIterator_copy(self)

    def next(self):
        return _pyupm_kx122.SwigPyIterator_next(self)

    def __next__(self):
        return _pyupm_kx122.SwigPyIterator___next__(self)

    def previous(self):
        return _pyupm_kx122.SwigPyIterator_previous(self)

    def advance(self, n):
        return _pyupm_kx122.SwigPyIterator_advance(self, n)

    def __eq__(self, x):
        return _pyupm_kx122.SwigPyIterator___eq__(self, x)

    def __ne__(self, x):
        return _pyupm_kx122.SwigPyIterator___ne__(self, x)

    def __iadd__(self, n):
        return _pyupm_kx122.SwigPyIterator___iadd__(self, n)

    def __isub__(self, n):
        return _pyupm_kx122.SwigPyIterator___isub__(self, n)

    def __add__(self, n):
        return _pyupm_kx122.SwigPyIterator___add__(self, n)

    def __sub__(self, *args):
        return _pyupm_kx122.SwigPyIterator___sub__(self, *args)
    def __iter__(self):
        return self
SwigPyIterator_swigregister = _pyupm_kx122.SwigPyIterator_swigregister
SwigPyIterator_swigregister(SwigPyIterator)

class floatVector(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, floatVector, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, floatVector, name)
    __repr__ = _swig_repr

    def iterator(self):
        return _pyupm_kx122.floatVector_iterator(self)
    def __iter__(self):
        return self.iterator()

    def __nonzero__(self):
        return _pyupm_kx122.floatVector___nonzero__(self)

    def __bool__(self):
        return _pyupm_kx122.floatVector___bool__(self)

    def __len__(self):
        return _pyupm_kx122.floatVector___len__(self)

    def __getslice__(self, i, j):
        return _pyupm_kx122.floatVector___getslice__(self, i, j)

    def __setslice__(self, *args):
        return _pyupm_kx122.floatVector___setslice__(self, *args)

    def __delslice__(self, i, j):
        return _pyupm_kx122.floatVector___delslice__(self, i, j)

    def __delitem__(self, *args):
        return _pyupm_kx122.floatVector___delitem__(self, *args)

    def __getitem__(self, *args):
        return _pyupm_kx122.floatVector___getitem__(self, *args)

    def __setitem__(self, *args):
        return _pyupm_kx122.floatVector___setitem__(self, *args)

    def pop(self):
        return _pyupm_kx122.floatVector_pop(self)

    def append(self, x):
        return _pyupm_kx122.floatVector_append(self, x)

    def empty(self):
        return _pyupm_kx122.floatVector_empty(self)

    def size(self):
        return _pyupm_kx122.floatVector_size(self)

    def swap(self, v):
        return _pyupm_kx122.floatVector_swap(self, v)

    def begin(self):
        return _pyupm_kx122.floatVector_begin(self)

    def end(self):
        return _pyupm_kx122.floatVector_end(self)

    def rbegin(self):
        return _pyupm_kx122.floatVector_rbegin(self)

    def rend(self):
        return _pyupm_kx122.floatVector_rend(self)

    def clear(self):
        return _pyupm_kx122.floatVector_clear(self)

    def get_allocator(self):
        return _pyupm_kx122.floatVector_get_allocator(self)

    def pop_back(self):
        return _pyupm_kx122.floatVector_pop_back(self)

    def erase(self, *args):
        return _pyupm_kx122.floatVector_erase(self, *args)

    def __init__(self, *args):
        this = _pyupm_kx122.new_floatVector(*args)
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this

    def push_back(self, x):
        return _pyupm_kx122.floatVector_push_back(self, x)

    def front(self):
        return _pyupm_kx122.floatVector_front(self)

    def back(self):
        return _pyupm_kx122.floatVector_back(self)

    def assign(self, n, x):
        return _pyupm_kx122.floatVector_assign(self, n, x)

    def resize(self, *args):
        return _pyupm_kx122.floatVector_resize(self, *args)

    def insert(self, *args):
        return _pyupm_kx122.floatVector_insert(self, *args)

    def reserve(self, n):
        return _pyupm_kx122.floatVector_reserve(self, n)

    def capacity(self):
        return _pyupm_kx122.floatVector_capacity(self)
    __swig_destroy__ = _pyupm_kx122.delete_floatVector
    __del__ = lambda self: None
floatVector_swigregister = _pyupm_kx122.floatVector_swigregister
floatVector_swigregister(floatVector)

DEFAULT_SPI_FREQUENCY = _pyupm_kx122.DEFAULT_SPI_FREQUENCY
KX122_DEFAULT_SLAVE_ADDR_1 = _pyupm_kx122.KX122_DEFAULT_SLAVE_ADDR_1
KX122_DEFAULT_SLAVE_ADDR_2 = _pyupm_kx122.KX122_DEFAULT_SLAVE_ADDR_2
MICRO_S = _pyupm_kx122.MICRO_S
KX122_ODR_12P5 = _pyupm_kx122.KX122_ODR_12P5
KX122_ODR_25 = _pyupm_kx122.KX122_ODR_25
KX122_ODR_50 = _pyupm_kx122.KX122_ODR_50
KX122_ODR_100 = _pyupm_kx122.KX122_ODR_100
KX122_ODR_200 = _pyupm_kx122.KX122_ODR_200
KX122_ODR_400 = _pyupm_kx122.KX122_ODR_400
KX122_ODR_800 = _pyupm_kx122.KX122_ODR_800
KX122_ODR_1600 = _pyupm_kx122.KX122_ODR_1600
KX122_ODR_0P781 = _pyupm_kx122.KX122_ODR_0P781
KX122_ODR_1P563 = _pyupm_kx122.KX122_ODR_1P563
KX122_ODR_3P125 = _pyupm_kx122.KX122_ODR_3P125
KX122_ODR_6P25 = _pyupm_kx122.KX122_ODR_6P25
KX122_ODR_3200 = _pyupm_kx122.KX122_ODR_3200
KX122_ODR_6400 = _pyupm_kx122.KX122_ODR_6400
KX122_ODR_12800 = _pyupm_kx122.KX122_ODR_12800
KX122_ODR_25600 = _pyupm_kx122.KX122_ODR_25600
KX122_NO_AVG = _pyupm_kx122.KX122_NO_AVG
KX122_2_SAMPLE_AVG = _pyupm_kx122.KX122_2_SAMPLE_AVG
KX122_4_SAMPLE_AVG = _pyupm_kx122.KX122_4_SAMPLE_AVG
KX122_8_SAMPLE_AVG = _pyupm_kx122.KX122_8_SAMPLE_AVG
KX122_16_SAMPLE_AVG = _pyupm_kx122.KX122_16_SAMPLE_AVG
KX122_32_SAMPLE_AVG = _pyupm_kx122.KX122_32_SAMPLE_AVG
KX122_64_SAMPLE_AVG = _pyupm_kx122.KX122_64_SAMPLE_AVG
KX122_128_SAMPLE_AVG = _pyupm_kx122.KX122_128_SAMPLE_AVG
KX122_RANGE_2G = _pyupm_kx122.KX122_RANGE_2G
KX122_RANGE_4G = _pyupm_kx122.KX122_RANGE_4G
KX122_RANGE_8G = _pyupm_kx122.KX122_RANGE_8G
KX122_FIFO_MODE = _pyupm_kx122.KX122_FIFO_MODE
KX122_FILO_MODE = _pyupm_kx122.KX122_FILO_MODE
KX122_STREAM_MODE = _pyupm_kx122.KX122_STREAM_MODE
KX122_BUF_FULL_INT = _pyupm_kx122.KX122_BUF_FULL_INT
KX122_WATERMARK_INT = _pyupm_kx122.KX122_WATERMARK_INT
KX122_DATA_READY_INT = _pyupm_kx122.KX122_DATA_READY_INT
HIGH_RES = _pyupm_kx122.HIGH_RES
LOW_RES = _pyupm_kx122.LOW_RES
ODR_9 = _pyupm_kx122.ODR_9
ODR_2 = _pyupm_kx122.ODR_2
INT1 = _pyupm_kx122.INT1
INT2 = _pyupm_kx122.INT2
ACTIVE_LOW = _pyupm_kx122.ACTIVE_LOW
ACTIVE_HIGH = _pyupm_kx122.ACTIVE_HIGH
class _kx122_context(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, _kx122_context, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, _kx122_context, name)
    __repr__ = _swig_repr
    __swig_setmethods__["i2c"] = _pyupm_kx122._kx122_context_i2c_set
    __swig_getmethods__["i2c"] = _pyupm_kx122._kx122_context_i2c_get
    if _newclass:
        i2c = _swig_property(_pyupm_kx122._kx122_context_i2c_get, _pyupm_kx122._kx122_context_i2c_set)
    __swig_setmethods__["spi"] = _pyupm_kx122._kx122_context_spi_set
    __swig_getmethods__["spi"] = _pyupm_kx122._kx122_context_spi_get
    if _newclass:
        spi = _swig_property(_pyupm_kx122._kx122_context_spi_get, _pyupm_kx122._kx122_context_spi_set)
    __swig_setmethods__["gpio1"] = _pyupm_kx122._kx122_context_gpio1_set
    __swig_getmethods__["gpio1"] = _pyupm_kx122._kx122_context_gpio1_get
    if _newclass:
        gpio1 = _swig_property(_pyupm_kx122._kx122_context_gpio1_get, _pyupm_kx122._kx122_context_gpio1_set)
    __swig_setmethods__["gpio2"] = _pyupm_kx122._kx122_context_gpio2_set
    __swig_getmethods__["gpio2"] = _pyupm_kx122._kx122_context_gpio2_get
    if _newclass:
        gpio2 = _swig_property(_pyupm_kx122._kx122_context_gpio2_get, _pyupm_kx122._kx122_context_gpio2_set)
    __swig_setmethods__["chip_select"] = _pyupm_kx122._kx122_context_chip_select_set
    __swig_getmethods__["chip_select"] = _pyupm_kx122._kx122_context_chip_select_get
    if _newclass:
        chip_select = _swig_property(_pyupm_kx122._kx122_context_chip_select_get, _pyupm_kx122._kx122_context_chip_select_set)
    __swig_setmethods__["accel_scale"] = _pyupm_kx122._kx122_context_accel_scale_set
    __swig_getmethods__["accel_scale"] = _pyupm_kx122._kx122_context_accel_scale_get
    if _newclass:
        accel_scale = _swig_property(_pyupm_kx122._kx122_context_accel_scale_get, _pyupm_kx122._kx122_context_accel_scale_set)
    __swig_setmethods__["res_mode"] = _pyupm_kx122._kx122_context_res_mode_set
    __swig_getmethods__["res_mode"] = _pyupm_kx122._kx122_context_res_mode_get
    if _newclass:
        res_mode = _swig_property(_pyupm_kx122._kx122_context_res_mode_get, _pyupm_kx122._kx122_context_res_mode_set)
    __swig_setmethods__["grange_mode"] = _pyupm_kx122._kx122_context_grange_mode_set
    __swig_getmethods__["grange_mode"] = _pyupm_kx122._kx122_context_grange_mode_get
    if _newclass:
        grange_mode = _swig_property(_pyupm_kx122._kx122_context_grange_mode_get, _pyupm_kx122._kx122_context_grange_mode_set)
    __swig_setmethods__["buffer_accel_scale"] = _pyupm_kx122._kx122_context_buffer_accel_scale_set
    __swig_getmethods__["buffer_accel_scale"] = _pyupm_kx122._kx122_context_buffer_accel_scale_get
    if _newclass:
        buffer_accel_scale = _swig_property(_pyupm_kx122._kx122_context_buffer_accel_scale_get, _pyupm_kx122._kx122_context_buffer_accel_scale_set)
    __swig_setmethods__["buffer_mode"] = _pyupm_kx122._kx122_context_buffer_mode_set
    __swig_getmethods__["buffer_mode"] = _pyupm_kx122._kx122_context_buffer_mode_get
    if _newclass:
        buffer_mode = _swig_property(_pyupm_kx122._kx122_context_buffer_mode_get, _pyupm_kx122._kx122_context_buffer_mode_set)
    __swig_setmethods__["buffer_res"] = _pyupm_kx122._kx122_context_buffer_res_set
    __swig_getmethods__["buffer_res"] = _pyupm_kx122._kx122_context_buffer_res_get
    if _newclass:
        buffer_res = _swig_property(_pyupm_kx122._kx122_context_buffer_res_get, _pyupm_kx122._kx122_context_buffer_res_set)
    __swig_setmethods__["using_spi"] = _pyupm_kx122._kx122_context_using_spi_set
    __swig_getmethods__["using_spi"] = _pyupm_kx122._kx122_context_using_spi_get
    if _newclass:
        using_spi = _swig_property(_pyupm_kx122._kx122_context_using_spi_get, _pyupm_kx122._kx122_context_using_spi_set)

    def __init__(self):
        this = _pyupm_kx122.new__kx122_context()
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _pyupm_kx122.delete__kx122_context
    __del__ = lambda self: None
_kx122_context_swigregister = _pyupm_kx122._kx122_context_swigregister
_kx122_context_swigregister(_kx122_context)

class odr_item(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, odr_item, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, odr_item, name)
    __repr__ = _swig_repr
    __swig_setmethods__["odr_value"] = _pyupm_kx122.odr_item_odr_value_set
    __swig_getmethods__["odr_value"] = _pyupm_kx122.odr_item_odr_value_get
    if _newclass:
        odr_value = _swig_property(_pyupm_kx122.odr_item_odr_value_get, _pyupm_kx122.odr_item_odr_value_set)
    __swig_setmethods__["odr_decimal"] = _pyupm_kx122.odr_item_odr_decimal_set
    __swig_getmethods__["odr_decimal"] = _pyupm_kx122.odr_item_odr_decimal_get
    if _newclass:
        odr_decimal = _swig_property(_pyupm_kx122.odr_item_odr_decimal_get, _pyupm_kx122.odr_item_odr_decimal_set)

    def __init__(self):
        this = _pyupm_kx122.new_odr_item()
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _pyupm_kx122.delete_odr_item
    __del__ = lambda self: None
odr_item_swigregister = _pyupm_kx122.odr_item_swigregister
odr_item_swigregister(odr_item)


def kx122_init(bus, addr, chip_select_pin, spi_bus_frequency):
    return _pyupm_kx122.kx122_init(bus, addr, chip_select_pin, spi_bus_frequency)
kx122_init = _pyupm_kx122.kx122_init

def kx122_close(dev):
    return _pyupm_kx122.kx122_close(dev)
kx122_close = _pyupm_kx122.kx122_close

def kx122_device_init(dev, odr, res, grange):
    return _pyupm_kx122.kx122_device_init(dev, odr, res, grange)
kx122_device_init = _pyupm_kx122.kx122_device_init

def kx122_get_sample_period(dev):
    return _pyupm_kx122.kx122_get_sample_period(dev)
kx122_get_sample_period = _pyupm_kx122.kx122_get_sample_period

def kx122_get_who_am_i(dev, data):
    return _pyupm_kx122.kx122_get_who_am_i(dev, data)
kx122_get_who_am_i = _pyupm_kx122.kx122_get_who_am_i

def kx122_get_acceleration_data_raw(dev):
    return _pyupm_kx122.kx122_get_acceleration_data_raw(dev)
kx122_get_acceleration_data_raw = _pyupm_kx122.kx122_get_acceleration_data_raw

def kx122_get_acceleration_data(dev):
    return _pyupm_kx122.kx122_get_acceleration_data(dev)
kx122_get_acceleration_data = _pyupm_kx122.kx122_get_acceleration_data

def kx122_sensor_software_reset(dev):
    return _pyupm_kx122.kx122_sensor_software_reset(dev)
kx122_sensor_software_reset = _pyupm_kx122.kx122_sensor_software_reset

def kx122_enable_iir(dev):
    return _pyupm_kx122.kx122_enable_iir(dev)
kx122_enable_iir = _pyupm_kx122.kx122_enable_iir

def kx122_disable_iir(dev):
    return _pyupm_kx122.kx122_disable_iir(dev)
kx122_disable_iir = _pyupm_kx122.kx122_disable_iir

def kx122_self_test(dev):
    return _pyupm_kx122.kx122_self_test(dev)
kx122_self_test = _pyupm_kx122.kx122_self_test

def kx122_set_sensor_standby(dev):
    return _pyupm_kx122.kx122_set_sensor_standby(dev)
kx122_set_sensor_standby = _pyupm_kx122.kx122_set_sensor_standby

def kx122_set_sensor_active(dev):
    return _pyupm_kx122.kx122_set_sensor_active(dev)
kx122_set_sensor_active = _pyupm_kx122.kx122_set_sensor_active

def kx122_set_odr(dev, odr):
    return _pyupm_kx122.kx122_set_odr(dev, odr)
kx122_set_odr = _pyupm_kx122.kx122_set_odr

def kx122_set_grange(dev, grange):
    return _pyupm_kx122.kx122_set_grange(dev, grange)
kx122_set_grange = _pyupm_kx122.kx122_set_grange

def kx122_set_resolution(dev, res):
    return _pyupm_kx122.kx122_set_resolution(dev, res)
kx122_set_resolution = _pyupm_kx122.kx122_set_resolution

def kx122_set_bw(dev, lpro):
    return _pyupm_kx122.kx122_set_bw(dev, lpro)
kx122_set_bw = _pyupm_kx122.kx122_set_bw

def kx122_set_average(dev, avg):
    return _pyupm_kx122.kx122_set_average(dev, avg)
kx122_set_average = _pyupm_kx122.kx122_set_average

def kx122_install_isr(dev, edge, intp, pin, isr, arg):
    return _pyupm_kx122.kx122_install_isr(dev, edge, intp, pin, isr, arg)
kx122_install_isr = _pyupm_kx122.kx122_install_isr

def kx122_uninstall_isr(dev, intp):
    return _pyupm_kx122.kx122_uninstall_isr(dev, intp)
kx122_uninstall_isr = _pyupm_kx122.kx122_uninstall_isr

def kx122_enable_interrupt1(dev, polarity):
    return _pyupm_kx122.kx122_enable_interrupt1(dev, polarity)
kx122_enable_interrupt1 = _pyupm_kx122.kx122_enable_interrupt1

def kx122_enable_interrupt2(dev, polarity):
    return _pyupm_kx122.kx122_enable_interrupt2(dev, polarity)
kx122_enable_interrupt2 = _pyupm_kx122.kx122_enable_interrupt2

def kx122_disable_interrupt1(dev):
    return _pyupm_kx122.kx122_disable_interrupt1(dev)
kx122_disable_interrupt1 = _pyupm_kx122.kx122_disable_interrupt1

def kx122_disable_interrupt2(dev):
    return _pyupm_kx122.kx122_disable_interrupt2(dev)
kx122_disable_interrupt2 = _pyupm_kx122.kx122_disable_interrupt2

def kx122_route_interrupt1(dev, bits):
    return _pyupm_kx122.kx122_route_interrupt1(dev, bits)
kx122_route_interrupt1 = _pyupm_kx122.kx122_route_interrupt1

def kx122_route_interrupt2(dev, bits):
    return _pyupm_kx122.kx122_route_interrupt2(dev, bits)
kx122_route_interrupt2 = _pyupm_kx122.kx122_route_interrupt2

def kx122_get_interrupt_status(dev):
    return _pyupm_kx122.kx122_get_interrupt_status(dev)
kx122_get_interrupt_status = _pyupm_kx122.kx122_get_interrupt_status

def kx122_get_interrupt_source(dev, data):
    return _pyupm_kx122.kx122_get_interrupt_source(dev, data)
kx122_get_interrupt_source = _pyupm_kx122.kx122_get_interrupt_source

def kx122_clear_interrupt(dev):
    return _pyupm_kx122.kx122_clear_interrupt(dev)
kx122_clear_interrupt = _pyupm_kx122.kx122_clear_interrupt

def kx122_enable_data_ready_interrupt(dev):
    return _pyupm_kx122.kx122_enable_data_ready_interrupt(dev)
kx122_enable_data_ready_interrupt = _pyupm_kx122.kx122_enable_data_ready_interrupt

def kx122_disable_data_ready_interrupt(dev):
    return _pyupm_kx122.kx122_disable_data_ready_interrupt(dev)
kx122_disable_data_ready_interrupt = _pyupm_kx122.kx122_disable_data_ready_interrupt

def kx122_enable_buffer_full_interrupt(dev):
    return _pyupm_kx122.kx122_enable_buffer_full_interrupt(dev)
kx122_enable_buffer_full_interrupt = _pyupm_kx122.kx122_enable_buffer_full_interrupt

def kx122_disable_buffer_full_interrupt(dev):
    return _pyupm_kx122.kx122_disable_buffer_full_interrupt(dev)
kx122_disable_buffer_full_interrupt = _pyupm_kx122.kx122_disable_buffer_full_interrupt

def kx122_enable_buffer(dev):
    return _pyupm_kx122.kx122_enable_buffer(dev)
kx122_enable_buffer = _pyupm_kx122.kx122_enable_buffer

def kx122_disable_buffer(dev):
    return _pyupm_kx122.kx122_disable_buffer(dev)
kx122_disable_buffer = _pyupm_kx122.kx122_disable_buffer

def kx122_buffer_init(dev, samples, res, mode):
    return _pyupm_kx122.kx122_buffer_init(dev, samples, res, mode)
kx122_buffer_init = _pyupm_kx122.kx122_buffer_init

def kx122_set_buffer_resolution(dev, res):
    return _pyupm_kx122.kx122_set_buffer_resolution(dev, res)
kx122_set_buffer_resolution = _pyupm_kx122.kx122_set_buffer_resolution

def kx122_set_buffer_threshold(dev, samples):
    return _pyupm_kx122.kx122_set_buffer_threshold(dev, samples)
kx122_set_buffer_threshold = _pyupm_kx122.kx122_set_buffer_threshold

def kx122_set_buffer_mode(dev, mode):
    return _pyupm_kx122.kx122_set_buffer_mode(dev, mode)
kx122_set_buffer_mode = _pyupm_kx122.kx122_set_buffer_mode

def kx122_get_buffer_status(dev, samples):
    return _pyupm_kx122.kx122_get_buffer_status(dev, samples)
kx122_get_buffer_status = _pyupm_kx122.kx122_get_buffer_status

def kx122_read_buffer_samples_raw(dev, len, x_array, y_array, z_array):
    return _pyupm_kx122.kx122_read_buffer_samples_raw(dev, len, x_array, y_array, z_array)
kx122_read_buffer_samples_raw = _pyupm_kx122.kx122_read_buffer_samples_raw

def kx122_read_buffer_samples(dev, len, x_array, y_array, z_array):
    return _pyupm_kx122.kx122_read_buffer_samples(dev, len, x_array, y_array, z_array)
kx122_read_buffer_samples = _pyupm_kx122.kx122_read_buffer_samples

def kx122_clear_buffer(dev):
    return _pyupm_kx122.kx122_clear_buffer(dev)
kx122_clear_buffer = _pyupm_kx122.kx122_clear_buffer
class KX122(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, KX122, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, KX122, name)
    __repr__ = _swig_repr

    def __init__(self, bus, addr, chip_select, spi_bus_frequency=10000):
        this = _pyupm_kx122.new_KX122(bus, addr, chip_select, spi_bus_frequency)
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _pyupm_kx122.delete_KX122
    __del__ = lambda self: None

    def deviceInit(self, odr, res, grange):
        return _pyupm_kx122.KX122_deviceInit(self, odr, res, grange)

    def getSamplePeriod(self):
        return _pyupm_kx122.KX122_getSamplePeriod(self)

    def getWhoAmI(self):
        return _pyupm_kx122.KX122_getWhoAmI(self)

    def getRawAccelerationData(self):
        return _pyupm_kx122.KX122_getRawAccelerationData(self)

    def getAccelerationData(self):
        return _pyupm_kx122.KX122_getAccelerationData(self)

    def getAccelerationDataVector(self):
        return _pyupm_kx122.KX122_getAccelerationDataVector(self)

    def softwareReset(self):
        return _pyupm_kx122.KX122_softwareReset(self)

    def enableIIR(self):
        return _pyupm_kx122.KX122_enableIIR(self)

    def disableIIR(self):
        return _pyupm_kx122.KX122_disableIIR(self)

    def selfTest(self):
        return _pyupm_kx122.KX122_selfTest(self)

    def setSensorStandby(self):
        return _pyupm_kx122.KX122_setSensorStandby(self)

    def setSensorActive(self):
        return _pyupm_kx122.KX122_setSensorActive(self)

    def setODR(self, odr):
        return _pyupm_kx122.KX122_setODR(self, odr)

    def setGrange(self, grange):
        return _pyupm_kx122.KX122_setGrange(self, grange)

    def setResolution(self, res):
        return _pyupm_kx122.KX122_setResolution(self, res)

    def setBW(self, lpro):
        return _pyupm_kx122.KX122_setBW(self, lpro)

    def setAverage(self, avg):
        return _pyupm_kx122.KX122_setAverage(self, avg)

    def installISR(self, edge, intp, pin, isr, arg):
        return _pyupm_kx122.KX122_installISR(self, edge, intp, pin, isr, arg)

    def uninstallISR(self, intp):
        return _pyupm_kx122.KX122_uninstallISR(self, intp)

    def enableInterrupt1(self, polarity):
        return _pyupm_kx122.KX122_enableInterrupt1(self, polarity)

    def enableInterrupt2(self, polarity):
        return _pyupm_kx122.KX122_enableInterrupt2(self, polarity)

    def disableInterrupt1(self):
        return _pyupm_kx122.KX122_disableInterrupt1(self)

    def disableInterrupt2(self):
        return _pyupm_kx122.KX122_disableInterrupt2(self)

    def routeInterrupt1(self, bits):
        return _pyupm_kx122.KX122_routeInterrupt1(self, bits)

    def routeInterrupt2(self, bits):
        return _pyupm_kx122.KX122_routeInterrupt2(self, bits)

    def getInterruptStatus(self):
        return _pyupm_kx122.KX122_getInterruptStatus(self)

    def getInterruptSource(self):
        return _pyupm_kx122.KX122_getInterruptSource(self)

    def clearInterrupt(self):
        return _pyupm_kx122.KX122_clearInterrupt(self)

    def enableDataReadyInterrupt(self):
        return _pyupm_kx122.KX122_enableDataReadyInterrupt(self)

    def disableDataReadyInterrupt(self):
        return _pyupm_kx122.KX122_disableDataReadyInterrupt(self)

    def enableBufferFullInterrupt(self):
        return _pyupm_kx122.KX122_enableBufferFullInterrupt(self)

    def disableBufferFullInterrupt(self):
        return _pyupm_kx122.KX122_disableBufferFullInterrupt(self)

    def enableBuffer(self):
        return _pyupm_kx122.KX122_enableBuffer(self)

    def disableBuffer(self):
        return _pyupm_kx122.KX122_disableBuffer(self)

    def bufferInit(self, samples, res, mode):
        return _pyupm_kx122.KX122_bufferInit(self, samples, res, mode)

    def setBufferResolution(self, res):
        return _pyupm_kx122.KX122_setBufferResolution(self, res)

    def setBufferThreshold(self, samples):
        return _pyupm_kx122.KX122_setBufferThreshold(self, samples)

    def setBufferMode(self, mode):
        return _pyupm_kx122.KX122_setBufferMode(self, mode)

    def getBufferStatus(self):
        return _pyupm_kx122.KX122_getBufferStatus(self)

    def getRawBufferSamples(self, len):
        return _pyupm_kx122.KX122_getRawBufferSamples(self, len)

    def getBufferSamples(self, len):
        return _pyupm_kx122.KX122_getBufferSamples(self, len)

    def clearBuffer(self):
        return _pyupm_kx122.KX122_clearBuffer(self)
KX122_swigregister = _pyupm_kx122.KX122_swigregister
KX122_swigregister(KX122)

# This file is compatible with both classic and new-style classes.


