# This file was automatically generated by SWIG (http://www.swig.org).
# Version 3.0.10
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.





from sys import version_info as _swig_python_version_info
if _swig_python_version_info >= (2, 7, 0):
    def swig_import_helper():
        import importlib
        pkg = __name__.rpartition('.')[0]
        mname = '.'.join((pkg, '_pyupm_vcap')).lstrip('.')
        try:
            return importlib.import_module(mname)
        except ImportError:
            return importlib.import_module('_pyupm_vcap')
    _pyupm_vcap = swig_import_helper()
    del swig_import_helper
elif _swig_python_version_info >= (2, 6, 0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_pyupm_vcap', [dirname(__file__)])
        except ImportError:
            import _pyupm_vcap
            return _pyupm_vcap
        if fp is not None:
            try:
                _mod = imp.load_module('_pyupm_vcap', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _pyupm_vcap = swig_import_helper()
    del swig_import_helper
else:
    import _pyupm_vcap
del _swig_python_version_info
try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if (name == "thisown"):
        return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if (not static):
        if _newclass:
            object.__setattr__(self, name, value)
        else:
            self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr(self, class_type, name):
    if (name == "thisown"):
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    raise AttributeError("'%s' object has no attribute '%s'" % (class_type.__name__, name))


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except __builtin__.Exception:
    class _object:
        pass
    _newclass = 0


def getVersion():
    return _pyupm_vcap.getVersion()
getVersion = _pyupm_vcap.getVersion
VCAP_DEFAULT_VIDEODEV = _pyupm_vcap.VCAP_DEFAULT_VIDEODEV
VCAP_DEFAULT_OUTPUTFILE = _pyupm_vcap.VCAP_DEFAULT_OUTPUTFILE
VCAP_DEFAULT_WIDTH = _pyupm_vcap.VCAP_DEFAULT_WIDTH
VCAP_DEFAULT_HEIGHT = _pyupm_vcap.VCAP_DEFAULT_HEIGHT
VCAP_DEFAULT_JPEG_QUALITY = _pyupm_vcap.VCAP_DEFAULT_JPEG_QUALITY
class VCAP(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, VCAP, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, VCAP, name)
    __repr__ = _swig_repr

    def __init__(self, *args):
        this = _pyupm_vcap.new_VCAP(*args)
        try:
            self.this.append(this)
        except __builtin__.Exception:
            self.this = this
    __swig_destroy__ = _pyupm_vcap.delete_VCAP
    __del__ = lambda self: None

    def setResolution(self, width, height):
        return _pyupm_vcap.VCAP_setResolution(self, width, height)

    def captureImage(self):
        return _pyupm_vcap.VCAP_captureImage(self)

    def saveImage(self, *args):
        return _pyupm_vcap.VCAP_saveImage(self, *args)

    def getWidth(self):
        return _pyupm_vcap.VCAP_getWidth(self)

    def getHeight(self):
        return _pyupm_vcap.VCAP_getHeight(self)

    def setJPGQuality(self, quality):
        return _pyupm_vcap.VCAP_setJPGQuality(self, quality)

    def getJPGQuality(self):
        return _pyupm_vcap.VCAP_getJPGQuality(self)

    def setDebug(self, enable):
        return _pyupm_vcap.VCAP_setDebug(self, enable)
VCAP_swigregister = _pyupm_vcap.VCAP_swigregister
VCAP_swigregister(VCAP)

# This file is compatible with both classic and new-style classes.


