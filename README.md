# OBC toolchain

The HERCULES OBSW (On-Board Software) was written in C11 and C++17, thereby, building the OBSW requires a compiler supporting both versions of these programming languages. The OBSW is deployed on a [RPi model 4B](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/) running a [RPi OS distribution](https://www.raspberrypi.com/software/). Compiling the source code in the RPi is not an option, since it is a very *slow and tedious* process.

That is why, the [ARM-Linux gcc and g++ cross-compilers](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/downloads) for Linux have been used. However, we have installed additional libraries in the RPi such as [pigpio](http://abyz.me.uk/rpi/pigpio/) or [gtest](https://github.com/google/googletest) which are not included with this cross compiler. To keep with the *cross compilation* alternative, we had to extract the rootfs from the RPi with all the required dependencies and tell the cross-compiler to use this rootfs as the *sysroot*.

The OBSW has been organized and developed with the CMake tool-set. Hence, we have used the CMake capabilities for cross compilation creating a *CMake-toolchain*, specified in the [`RPi.cmake`](RPi.cmake) file.

This repository contains the extracted rootfs mentioned before, in addition to the CMake configuration file to integrate this toolchain in the building process.

## Cross-compilation process

The following data-flow diagram depicts the high-level process for the cross compilation of our application sources. This is process is automated by CMake, hiding us from the *low-level* activities such as the creation of the static libraries, dependency management, etc. Although, it is of vital importance to understand what is happening under the hood:

<img src="docs/cross_compilation_process.png" width="600px">

Note that the we are using one static library per software component. This way, we can take advante of the **separate** (but not independent) compilation. A key feature for software decomposition and modular programming [^1].

[^1]: [Separate compilation in Ada](http://www.cs.uni.edu/~mccormic/AdaEssentials/compilation.htm). This is similar, but to a lesser extent, for C/C++ since the specification (header or `.h`) and body (source or `.c`) are treated as separate entities.
