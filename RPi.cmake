################################################################################
#                              H E R C C U L E S                               #
#            Copyright (C) 2022 Universidad Politécnica de Madrid              #
#                                                                              #
# This file have been developed by the real time systems group from UPM        #
################################################################################

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

# Use this .cmake location with ${CMAKE_CURRENT_LIST_DIR} to access the rootfs:
set(CMAKE_FIND_ROOT_PATH ${CMAKE_CURRENT_LIST_DIR}/rpi_rootfs)
set(CMAKE_SYSROOT ${CMAKE_FIND_ROOT_PATH})

set(CMAKE_C_COMPILER "arm-linux-gnueabihf-gcc")
set(CMAKE_C_COMPILER_TARGET ${arch})
set(CMAKE_CXX_COMPILER "arm-linux-gnueabihf-g++")
set(CMAKE_CXX_COMPILER_TARGET ${arch})

SET(CMAKE_EXE_LINKER_FLAGS    "${CMAKE_EXE_LINKER_FLAGS} --sysroot=${CMAKE_FIND_ROOT_PATH}")
SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} --sysroot=${CMAKE_FIND_ROOT_PATH}")
SET(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} --sysroot=${CMAKE_FIND_ROOT_PATH}")

# Search for programs only in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# Search for libraries and headers only in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

message(STATUS "[RPi.cmake] Using RPI rootfs!")
